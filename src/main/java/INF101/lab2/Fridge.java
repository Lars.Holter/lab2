package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private final int maxCapacity = 20;
    private ArrayList<FridgeItem> fridgeItems = new ArrayList<>(maxCapacity);

    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (totalSize()-nItemsInFridge() > 0) {
            fridgeItems.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!fridgeItems.remove(item)) {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeItems = new ArrayList<>(maxCapacity);
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>(maxCapacity);
        for (FridgeItem item : fridgeItems) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        fridgeItems.removeAll(expiredItems);
        return expiredItems;
    }
}
